## About Trails

[**Trails**](https://gitlab.tails.boum.org/eg/TRAILS1) is a portable operating system that protects your privacy and helps you avoid censorship.

[![Drawing of a Trails stick that is marked to be pluged into a labtop](https://tails.boum.org/index/laptop.svg)](https://gitlab.tails.boum.org/eg/TRAILS1)
- Trails uses the Tor network to protect your privacy online and help you avoid censorship. Enjoy the Internet like it should be.
- Shut down the computer and start on your Trails USB stick instead of starting on Windows, macOS, or Linux. Trails leaves no trace on the computer when shut down.
- Trails includes a selection of applications to work on sensitive documents and communicate securely. Everything in Trails is ready-to-use and has safe defaults.
- You can download Trails for free and independent security researchers can verify our work. Trails is based [Tails](https://tails.boum.org/)




### How to contribute to Trails

There are many ways you can contribute to Trails. No effort is too small and whatever you bring to this community will be appreciated.

Request edit aces


###  License and source code distribution
**Trails** is [Free Software](https://www.gnu.org/philosophy/free-sw.html): you can download, use, and share it with no restrictions.

 <a href="https://tails.boum.org/doc/about/license/"><img alt="Tails is Free Software" src="https://tails.boum.org/index/gift.svg" width="560"/>


However, Trails includes non-free firmware in order to work on as much hardware as possible.


