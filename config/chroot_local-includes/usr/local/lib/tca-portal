#!/usr/bin/python3
"""
This service listens on a unix domain socket and provides a simple interface to
run privileged actions.

It is meant to be run as root: in fact, some of the functionalities it is
exposing require root privileges. Some others are not (see UserCommand), so in
theory we could split this portal into a privileged one and an unprivileged
one. However, that wouldn't reduce the attack surface much, if at all.
"""

import re
import os
import sys
import stat
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import socketserver
from logging import getLogger
import socket
import time
from subprocess import Popen
import multiprocessing
from typing import List, Optional

from tinyrpc.protocols.jsonrpc import JSONRPCProtocol, JSONRPCParseError
from tinyrpc.protocols import RPCRequest, RPCResponse
from tinyrpc.exc import InvalidRequestError

import systemd.daemon

from tailslib.logutils import configure_logging
from tailslib.gnome import gnome_env_vars
from tailslib import (
    LIVE_USERNAME,
    PERSISTENCE_CONF_FILE,
    PERSISTENCE_SETUP_USERNAME,
)

log = getLogger("tca-portal")


# notes about the process model of tca-portal:
# - tca-portal runs a dedicated process for each incoming connection, thanks to ForkingUnixServer
# - each Handler spawns a dedicated process to read incoming data
# - if incoming data is valid, it will spawn a process to manage the actual command process. That's
#   Handler.run. This method will spawn a Popen. This is the process that the user is actually interested in.
#   Handler.run will wait for Popen to complete, get results, push data to a queue where Handler.handle will pick it up and send the response
#
# tca-portal
#  + Handler1
#    …
#  + Handler2
#  |-- reader
#  |-- + run
#  |   `-- Popen (the actual command that the user wants to run)
#  `-- + run
#      `-- Popen (the actual command that the user wants to run)


class FDUnixStreamServer(socketserver.UnixStreamServer):
    """
    This server can *both* use a previously activated fd, or bind a new one.

    To make it use a previously activated fd, just pass it as `fd` keyword parameter to the constructor.
    """

    def __init__(self, *args, **kwargs):
        self.fd: int = kwargs.pop("fd", None)
        if self.fd is not None:
            # Invoke base but omit bind/listen steps
            kwargs["bind_and_activate"] = False
        socketserver.UnixStreamServer.__init__(self, *args, **kwargs)
        if self.fd is not None:
            self.socket = socket.fromfd(self.fd, self.address_family, self.socket_type)


class ForkingUnixServer(socketserver.ForkingMixIn, FDUnixStreamServer):
    allow_reuse_address = True
    timeout = None


class InvalidArgsError(InvalidRequestError):
    pass


class BaseCommand:
    def __init__(self, cmd: List[str], arguments: bool):
        self.cmd = cmd
        self.arguments = arguments

    def validate_request(self, request: RPCRequest) -> None:
        '''
        raise if it's not valid
        '''
        self.full_command(request)

    def validate_args(self, args: list) -> list:
        '''
        Validate or update the command arguments.

        Raise an InvalidRequestError if the arguments are not valid

        This method is meant to be overridden, so that you can easily add validation to your Command
        '''
        if not self.arguments and args:
            raise InvalidArgsError("Arguments are not accepted for this command")
        return args

    def full_command(self, request: RPCRequest) -> list:
        cmd = self.cmd + self.validate_args(request.args)
        return cmd

    def run(self, request: RPCRequest):
        cmd = self.full_command(request)
        log.debug("Running %s", cmd)
        return Popen(cmd)


class RootCommand(BaseCommand):
    pass


class UserCommand(BaseCommand):
    def full_command(self, request: RPCRequest) -> list:
        drop = ["runuser", "-u", LIVE_USERNAME]
        env = ["env", *gnome_env_vars()]

        args = [*drop, "--", *env, *(super().full_command(request))]
        return args


class PersistenceSetupCommand(BaseCommand):
    def full_command(self, request: RPCRequest) -> list:
        drop = ["runuser", "-u", PERSISTENCE_SETUP_USERNAME]
        env = ["env", *gnome_env_vars()]

        args = [*drop, "--", *env, *(super().full_command(request))]
        return args


class SetTimeCommand(RootCommand):
    def __init__(self):
        super().__init__(['/usr/local/lib/tails-set-date'], True)
        # we know our argument is an ISO8601 date whose separator is a T, and with no microseconds
        self.re = re.compile(r'[0-9+:T-]*')

    def validate_args(self, args: list) -> list:
        if len(args) != 1:
            raise InvalidArgsError("Invalid number of arguments")
        # args[0] should be sth like 2021-08-06T17:38+00:00
        datestring = args[0]
        if len(datestring) > 22:
            raise InvalidArgsError("Datestring too long")
        if not self.re.fullmatch(datestring):
            raise InvalidArgsError("Datestring has invalid characters")
        return args


class Handler(socketserver.StreamRequestHandler):
    """
    This class implements the JSON-RPC line-protocol + some glue code to call relevant actions.

    The protocol is basically JSON-RPC with some more restrictions:
     - requests are in a single line
     - response are in a single line

    This is not supported at the moment, but could:
     - server-initiated messages (ie: signals)
    """

    MAX_LINESIZE = 1024

    spawn_commands = {  # some commands just mean "run this specified commands as LIVE_USERNAME, please"
        "open-unsafebrowser": UserCommand(["gtk-launch", "unsafe-browser"], False),
        "open-tbb": UserCommand(["gtk-launch", "tor-browser"], False),
        "open-networkmonitor": UserCommand(
            ["gnome-system-monitor", "--show-resources-tab"], False
        ),
        "open-onioncircuits": UserCommand(["gtk-launch", "onioncircuits"], False),
        "open-documentation": UserCommand(["tails-documentation"], True),
        "open-wifi-config": UserCommand(['systemctl', '--user', 'start', 'gnome-wifi-panel'], False),
        "set-tor-sandbox": RootCommand(['/usr/local/lib/tor-pt-configuration-helper'], True),
        # It would be nicer to do this in Python without calling out
        # to external processes, but it would require either implementing
        # the Popen interface that the portal assumes, or refactoring
        # the Handler.run and Command.run code a fair bit:
        # https://gitlab.tails.boum.org/tails/tails/-/merge_requests/544#note_174919
        "is-tor-configuration-persistent?": PersistenceSetupCommand(
            ["/usr/local/lib/tails-tor-config-persistence", "is-enabled"],
            False),
        "enable-tor-configuration-persistence": PersistenceSetupCommand(
            ["/usr/local/lib/tails-tor-config-persistence", "enable"],
            False),
        "disable-tor-configuration-persistence": PersistenceSetupCommand(
            ["/usr/local/lib/tails-tor-config-persistence", "disable"],
            False),
        "set-system-time": SetTimeCommand(),
    }
    valid_commands = set(spawn_commands.keys())

    protocol = JSONRPCProtocol()

    def handle_line(self, line: bytes) -> Optional[RPCResponse]:
        """
        Parse the line and do what's appropriate.

        Return an RPCResponse only if there is some error.
        Otherwise, will spawn a dedicated process. the outcome of it will be pushed to outgoing_q
        """
        try:
            req = self.protocol.parse_request(line)
        except JSONRPCParseError as exc:
            return exc.error_respond()
        except InvalidRequestError as exc:
            return exc.error_respond()

        try:
            return self.handle_request(req)
        except InvalidArgsError:
            log.error("Invalid arguments: %s", str(req.args))
            return req.error_respond(error="Invalid arguments")
        except InvalidRequestError:
            return req.error_respond(error="Invalid request")
        except Exception:
            log.exception("Error processing request %s", req)
            return req.error_respond(error="Server error")

    def handle_request(self, req: RPCRequest) -> Optional[RPCResponse]:
        if req.method not in self.valid_commands:
            return req.error_respond(error="Invalid method")
        if req.method not in self.spawn_commands:
            return req.error_respond(error="Method not implemented")

        cmd = self.spawn_commands[req.method]
        cmd.validate_request(req)
        p = multiprocessing.Process(target=self.run, args=(cmd, req))
        p.start()
        return None

    def run(self, cmd, req):
        """
        Spawn the intended cmd, then wait for it and put the appropriate response in the outgoing_q

        this method is intended to be run in a dedicated Process
        """
        process = cmd.run(req)
        process.communicate()
        if process.returncode == 0:
            response = req.respond(result={"returncode": process.returncode})
        else:
            # The library we use doesn't seem to support adding the optional "data" parameter to error
            # responses. We could definitely implement our own subclass, if we ever need that.
            response = req.error_respond(error="Command failed with returncode %d" % process.returncode)
        self.outgoing_q.put(response)

    def send(self, response: RPCResponse):
        log = getLogger("traffic")
        output = response.serialize()
        log.debug("-> %s", output)
        output += "\n"
        self.request.send(output.encode("utf8"))

    def reader(self):
        """
        Read from the socket. This method is intended to be run as a dedicated process.

        It reads from the socket, process the data accordingly, and pass it to handle_line (which will spawn
        dedicated processes for it).
        """
        log = getLogger("traffic")
        while True:
            peek_ahead = self.request.recv(self.MAX_LINESIZE, socket.MSG_PEEK)
            if not peek_ahead:
                return
            if b"\n" not in peek_ahead:
                time.sleep(0.1)
                continue
            request_length = peek_ahead.find(b"\n")
            line_in = self.request.recv(request_length + 1).rstrip(b"\n").decode("utf8")
            if not line_in:
                continue
            log.debug("<- %s", line_in)
            response = self.handle_line(line_in.lstrip())
            if response is not None:
                self.outgoing_q.put(response)

    def handle(self):

        self.pid = os.getpid()
        self.outgoing_q = multiprocessing.Queue()

        self.reader = multiprocessing.Process(target=self.reader)
        self.reader.start()

        while True:
            response = self.outgoing_q.get()
            self.send(response)


def main():
    if os.getuid() != 0:
        print("%s must be run as root" % sys.argv[0], file=sys.stderr)
        sys.exit(1)
    p = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    p.add_argument(
        "--systemd-socket",
        action="store_true",
        default=False,
        help="use systemd-activated socket",
    )
    p.add_argument(
        "--listen",
        metavar="SOCKET-PATH",
        default="/run/tca-portal.sock",
        help="Ignored if --systemd-socket is used",
    )
    p.add_argument(
        "--log-level",
        metavar="LEVEL",
        choices=["DEBUG", "INFO", "WARNING", "CRITICAL"],
        default="INFO",
    )
    args = p.parse_args()

    log_conf = {'level': args.log_level}
    configure_logging(hint='syslog', ident='tca-portal', **log_conf)

    if not args.systemd_socket:
        if os.path.exists(args.listen) and stat.S_ISSOCK(os.stat(args.listen).st_mode):
            os.remove(args.listen)
    if args.systemd_socket:
        available_fds = systemd.daemon.listen_fds()
        log.debug("Available fds: %s" % str(available_fds))
        if not available_fds:
            print("Error: no systemd-activated socket found", file=sys.stderr)
            sys.exit(1)
        elif len(available_fds) != 1:
            print("Error: number of FDs received is incorrect")
            sys.exit(1)
        listen_fd = available_fds[0]
    else:
        listen_fd = None
    with ForkingUnixServer(args.listen, Handler, fd=listen_fd) as server:
        if not args.systemd_socket:
            os.chmod(args.listen, 0o600)
            log.info("Listening on %s", args.listen)
        else:
            log.info("Serving on a systemd-activated socket")
        systemd.daemon.notify('READY=1')
        server.serve_forever()


if __name__ == "__main__":
    main()
